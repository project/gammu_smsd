# Gammu SMS Daemon

The Drupal Gammu SMS Module is a powerful tool designed to streamline the
process of sending SMS messages using the Gammu SMS Daemon (smsd) after
it has been installed.
Provides a user-friendly interface that enables users to easily connect
with their clients and send SMS messages to a list of phone numbers.
The primary purpose of this module is to simplify the composition and
sending of SMS messages through an intuitive web-based interface.

## Features

- User-Friendly Interface: The module offers a user-friendly web interface
that makes it easy for users to compose SMS messages and send them to
a list of phone numbers. This eliminates the need for complex command-line
interactions with Gammu SMS Daemon.

- Client Communication: Users can use the module to connect with their
clients effectively. Whether it's sending important updates, notifications,
or promotional content, the module facilitates seamless communication.

- Phone List Management: The module allows users to manage their phone number
lists conveniently. Users can import, edit, and maintain phone number lists
through the interface.

- Message Composition: Users can compose SMS messages directly within
the module's interface. The WYSIWYG editor ensures that messages are
composed and formatted accurately.

# Getting Started

Follow these steps to get started with the Drupal Gammu SMS Module:

1. **Install Gammu SMS Daemon:** Before using the module, ensure that
Gammu SMS Daemon (smsd) is properly installed and configured on your server.
It relies on the functionality provided by Gammu for sending SMS messages.

2. **Module Installation:** Install and enable the Drupal Gammu SMS Module,
just like any other Drupal module. You can download it from the Drupal module
repository and install it through the administrative interface.

3. **Configuration:** Configure the module by providing the necessary
information to connect it with your Gammu SMS Daemon installation.
This includes specifying the communication method, port, and any authentication
details required.

4. **Phone List Setup:** Import or manually add the phone numbers of
your intended recipients. This could be your client list, subscribers, or any
other group you want to communicate with.

5. **Compose and Send:** Start composing your SMS messages using
the module's interface. You can use the WYSIWYG editor to format your messages.
Once ready, select the recipients from your phone list and send the messages.
