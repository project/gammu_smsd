(function ($, Drupal, once) {
  Drupal.behaviors.gammu_smsd = {
    attach: function (context) {
      $(once('initiate-gammu-smsd', context === document ? 'html' : context)).each(function initOnce() {
        $('#edit-gammu-phonebook-phone--wrapper legend,#checkAll').on('click', function () {
          let statut = $(this).data('status');
          if(statut){
            $(this).data('status',0);
            $(this).closest('form').find(".js-form-type-checkbox:visible input:checkbox").prop('checked', false);
          } else {
            $(this).data('status',1);
            $(this).closest('form').find(".js-form-type-checkbox:visible input:checkbox").prop('checked', true);
          }
        });
        //search phonebook
        $("#edit-gammu-phonebook-search").keyup(function () {
          var txt = $(this).val();
          $(this).closest('details').find('.js-form-type-checkbox').hide();
          $(this).closest('details').find('.js-form-type-checkbox').each(function () {
            if ($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1) {
              $(this).show();
            }
          });
        });

        //search message
        $("#edit-search").keyup(function () {
          var txt = $(this).val();
          $(this).closest('form').find('.list-group-item').hide();
          $(this).closest('form').find('.list-group-item label').each(function () {
            if ($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1) {
              $(this).closest('.list-group-item').show();
            }
          });
        });

      });
    }
  };
}(jQuery, Drupal, once));
