<?php

namespace Drupal\gammu_smsd\Plugin\rest\resource;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\gammu_smsd\GammuDatabase;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides a resource to get inbox.
 *
 * @RestResource(
 *   id = "gammu_sentitems_resource",
 *   label = @Translation("Gammu sent messages"),
 *   uri_paths = {
 *     "canonical" = "/api/gammu/sent"
 *   }
 * )
 */
class GammuSentRessource extends ResourceBase {

  /**
   * Gammu Database.
   *
   * @var \Drupal\gammu_smsd\GammuDatabase
   */
  protected $gammuDB;

  /**
   * Gammu Database connection.
   *
   * @var mixed
   */
  protected $connection;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   A current user instance.
   * @param \Drupal\gammu_smsd\GammuDatabase $gammu_db
   *   Connection database gammu.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Configuration service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, protected AccountProxyInterface $currentUser, GammuDatabase $gammu_db, protected ConfigFactoryInterface $config) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->gammuDB = $gammu_db;
    $this->connection = $gammu_db->gammuConnection();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('gammu_rest'),
      $container->get('current_user'),
      $container->get('gammu_smsd.db'),
      $container->get('config.factory'),
    );
  }

  /**
   * Responds to GET requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get($data_telephone = []) {
    if (!$this->currentUser->hasPermission('Administer gammu sms')) {
      throw new AccessDeniedHttpException();
    }
    $config = $this->config->get('gammu_smsd.settings');

    $country_code = $config->get('gammu_country_code');
    $sql = "SELECT DestinationNumber, ID, SendingDateTime, UDH, TextDecoded FROM {sentitems} WHERE UDH = '' OR UDH LIKE '%1' ORDER BY SendingDateTime DESC";
    $results = $this->connection->query($sql);
    $result = [];
    if (!empty($results)) {
      foreach ($results as $message) {
        $textDecoded = trim($message->UDH) != '' ? $this->gammuDB->getMessageMultipart('sentitems', $message->UDH, $message->DestinationNumber) : $message->TextDecoded;
        $phone_num = str_replace($country_code, 0, $message->DestinationNumber);
        $phone_name = $data_telephone[$phone_num] ?? $phone_num;
        $result[$message->ID] = [
          'number' => $phone_num,
          'text' => $textDecoded,
          'name' => $phone_name,
        ];
      }
    }
    Database::setActiveConnection();

    $response = new ResourceResponse($result);
    $response->addCacheableDependency($result);
    return $response;
  }

}
