<?php

namespace Drupal\gammu_smsd\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\gammu_smsd\GammuSendSMS;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller routines for test_api routes.
 */
class GammuAPIController extends ControllerBase {

  /**
   * SmsSentController constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config service.
   * @param \Drupal\gammu_smsd\GammuSendSMS $gammuSendSMS
   *   The gammu send sms service.
   */
  public function __construct(protected ConfigFactoryInterface $config, protected GammuSendSMS $gammuSendSMS) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('gammu_smsd.sendSMS'),
    );
  }

  /**
   * Callback for `api/gammu/send` API method.
   */
  public function send(Request $request) {
    $config = $this->config->get('gammu_smsd.settings');
    $token = $config->get('gammu_token');
    $response = FALSE;
    if (str_starts_with($request->headers->get('Content-Type'), 'application/json') && $request->headers->get('Authorization') == $token) {
      $data = json_decode($request->getContent(), TRUE);
      $message = [$data['number'], $data['text']];
      $response = $this->gammuSendSMS->send([$message]);
    }
    return new JsonResponse($response);
  }

}
