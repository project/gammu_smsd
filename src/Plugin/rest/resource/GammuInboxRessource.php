<?php

namespace Drupal\gammu_smsd\Plugin\rest\resource;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\gammu_smsd\GammuDatabase;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides a resource to get inbox.
 *
 * @RestResource(
 *   id = "gammu_inbox_resource",
 *   label = @Translation("Gammu inbox"),
 *   uri_paths = {
 *     "canonical" = "/api/gammu/inbox"
 *   }
 * )
 */
class GammuInboxRessource extends ResourceBase {

  /**
   * Gammu Database connection.
   *
   * @var mixed
   */
  protected $connection;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   A current user instance.
   * @param \Drupal\gammu_smsd\GammuDatabase $gammuDB
   *   Connection database gammu.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Configuration service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, protected AccountProxyInterface $currentUser, protected GammuDatabase $gammuDB, protected ConfigFactoryInterface $config) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->connection = $this->gammuDB->gammuConnection();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('gammu_rest'),
      $container->get('current_user'),
      $container->get('gammu_smsd.db'),
      $container->get('config.factory'),
    );
  }

  /**
   * Responds to GET requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get() {
    if (!$this->currentUser->hasPermission('Administer gammu sms')) {
      throw new AccessDeniedHttpException();
    }
    $config = $this->config->get('gammu_smsd.settings');
    $country_code = $config->get('gammu_country_code');
    $data_telephone = $this->gammuDB->gammuGetPhonebook();
    Database::setActiveConnection('gammu_db');
    $sql = "SELECT ID,ReceivingDateTime, SenderNumber , UDH , TextDecoded FROM {inbox} WHERE UDH = '' OR UDH LIKE '%1' ORDER BY ReceivingDateTime DESC";
    $results = $this->connection->query($sql);
    $result = [];
    if (!empty($results)) {
      foreach ($results as $message) {
        $textDecoded = trim($message->UDH) != '' ? $this->gammuDB->getMessageMultipart('inbox', $message->UDH, $message->SenderNumber) : $message->TextDecoded;
        $phone_num = str_replace($country_code, 0, $message->SenderNumber);
        $phone_name = !empty($data_telephone[$phone_num]) ? $data_telephone[$phone_num] : $phone_num;
        $result[$message->ID] = [
          'number' => $phone_num,
          'text' => $textDecoded,
          'name' => $phone_name,
        ];
      }
    }
    Database::setActiveConnection();

    $response = new ResourceResponse($result);
    $response->addCacheableDependency($result);
    return $response;
  }

}
