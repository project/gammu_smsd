<?php

namespace Drupal\gammu_smsd\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\gammu_smsd\GammuDatabase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sms Search form.
 */
class SmsSearchForm extends FormBase {

  /**
   * Connection database gammu.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * Gammu smsd constructor.
   *
   * @param \Drupal\gammu_smsd\GammuDatabase $gammuDB
   *   Gammu database connection.
   */
  public function __construct(protected GammuDatabase $gammuDB) {
    $this->connection = $gammuDB->gammuConnection();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('gammu_smsd.db')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'sms_search_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $number = '') {
    $form['gammu_sms_search'] = [
      '#type' => 'search',
      '#required' => FALSE,
      '#default_value' => $number,
      '#attributes' => ['placeholder' => $this->t('Search')],
    ];
    if (!empty($number)) {
      $_SESSION['gammu_sms_search'] = $number;
    }
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
    ];
    if (!empty($input = $form_state->getUserInput())) {
      $_SESSION['gammu_sms_search'] = $input["gammu_sms_search"];
    }
    elseif (!empty($_SESSION['gammu_sms_search'])) {
      $form['gammu_sms_search']['#default_value'] = $search = $_SESSION['gammu_sms_search'];
      $config = $this->config('gammu_smsd.settings');
      $country_code = $config->get('gammu_country_code');
      $data_telephone = $this->gammuDB->gammuGetPhonebook();
      unset($_SESSION['gammu_sms_search']);
      if (is_numeric($search)) {
        $search = intval($search);
      }
      $search = '%' . addslashes($search) . '%';
      $sql_sentitems = "SELECT DestinationNumber as Number, ID, SendingDateTime as time,UDH, TextDecoded,'sentitems' as folder FROM {sentitems} WHERE (UDH = '' OR UDH LIKE '%1') AND CONCAT_WS('',DestinationNumber, TextDecoded) like '$search' ";
      $sql_inbox = "SELECT SenderNumber as Number, ID,ReceivingDateTime as time, UDH , TextDecoded, 'inbox' as folder FROM {inbox} WHERE (UDH = '' OR UDH LIKE '%1') AND CONCAT_WS('',SenderNumber, TextDecoded)  like '$search' ";

      $form['#attached']['library'][] = 'gammu_smsd/gammu_smsd';

      Database::setActiveConnection('gammu_db');
      $sql = $sql_sentitems . ' UNION ALL ' . $sql_inbox . ' ORDER BY time';
      $results = $this->connection->query($sql);
      if (!empty($results)) {
        $form['checkall'] = [
          '#markup' => '<div class="clearfix"><a id="checkAll" href="#checkAll" >✔ ' . $this->t('Check All') . '</a></div>',
        ];
        foreach ($results as $message) {
          $textDecoded = trim($message->UDH) != '' ? $this->gammuDB->getMessageMultipart($message->folder, $message->UDH, $message->Number) : $message->TextDecoded;
          if (empty($form[$message->folder])) {
            $form[$message->folder] = [
              '#type' => 'details',
              '#title' => $message->folder,
              '#open' => TRUE,
            ];
          }
          $phone_num = str_replace($country_code, 0, $message->Number);
          $number = $country_code . intval($phone_num);
          $phone_name = !empty($data_telephone[$phone_num]) ? $data_telephone[$phone_num] : $phone_num;
          $class = $message->folder == 'inbox' ? 'info' : 'primary';
          $form[$message->folder]["{$message->folder}[{$message->ID}]"] = [
            '#type' => 'checkbox',
            '#default_value' => $message->ID,
            '#title' => $textDecoded,
            '#description' => $this->gammuDB->gammuMessageDetail($number, $phone_name, $phone_num, $message->time),
            '#prefix' => '<div class="alert alert-' . $class . '" role="alert">',
            '#suffix' => '</div>',
          ];
        }
      }
      $form['actions']['delete'] = [
        '#type' => 'submit',
        '#value' => $this->t('Delete'),
      ];
    }
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $input = $form_state->getUserInput();
    if (!empty($input['op']) && $input['op'] == $this->t('Delete')) {
      foreach (['inbox', 'sentitems'] as $folder) {
        if (!empty($input[$folder])) {
          $this->gammuDB->gammuDeleteMessage($folder, array_keys($input[$folder]));
        }
      }
    }
  }

}
