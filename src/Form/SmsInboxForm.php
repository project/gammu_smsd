<?php

namespace Drupal\gammu_smsd\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\gammu_smsd\GammuDatabase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * SMS Inbox form.
 */
class SmsInboxForm extends FormBase {

  /**
   * Connection database gammu.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * Gammu smsd constructor.
   *
   * @param \Drupal\gammu_smsd\GammuDatabase $gammuDB
   *   Gammu database connection.
   */
  public function __construct(protected GammuDatabase $gammuDB) {
    $this->connection = $gammuDB->gammuConnection();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('gammu_smsd.db')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sms_inbox_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if (!$this->connection) {
      return FALSE;
    }
    $config = $this->config('gammu_smsd.settings');
    $country_code = $config->get('gammu_country_code');
    $data_telephone = $this->gammuDB->gammuGetPhonebook();
    Database::setActiveConnection('gammu_db');
    $sql = "SELECT ID,ReceivingDateTime, SenderNumber , UDH , TextDecoded FROM {inbox} WHERE UDH = '' OR UDH LIKE '%1' ORDER BY ReceivingDateTime DESC";
    $results = $this->connection->query($sql);
    if (!empty($results)) {
      $form['search'] = [
        '#type' => 'search',
        '#title' => '',
        '#attributes' => [
          'placeholder' => $this->t('Search'),
        ],
        '#suffix' => '<div id="checkAll"><a href="#checkAll">✔ ' . $this->t('Check All') . '</a></div>',
      ];
      $form['#attached']['library'][] = 'gammu_smsd/gammu_smsd';

      foreach ($results as $message) {
        $textDecoded = trim($message->UDH) != '' ? $this->gammuDB->getMessageMultipart('inbox', $message->UDH, $message->SenderNumber) : $message->TextDecoded;
        $phone_num = str_replace($country_code, 0, $message->SenderNumber);
        $number = $country_code . intval($phone_num);
        $phone_name = !empty($data_telephone[$phone_num]) ? $data_telephone[$phone_num] : $phone_num;
        $form[$message->ID] = [
          '#type' => 'checkbox',
          '#default_value' => $message->ID,
          '#title' => $textDecoded,
          '#description' => $this->gammuDB->gammuMessageDetail($number, $phone_name, $phone_num, $message->ReceivingDateTime),
          '#prefix' => '<div class="card mb-3 admin-item">',
          '#suffix' => '</div>',
          '#wrapper_attributes' => [
            'class' => ['card-body'],
          ],
        ];
      }
      $form['actions']['delete'] = [
        '#type' => 'submit',
        '#value' => $this->t('Delete'),
      ];
    }
    Database::setActiveConnection();
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $ids = array_filter($form_state->getValues());
    $ids = array_intersect_key($ids, array_flip(array_filter(array_keys($ids), 'is_numeric')));
    $this->gammuDB->gammuDeleteMessage('inbox', array_keys($ids));
  }

}
