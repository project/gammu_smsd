<?php

namespace Drupal\gammu_smsd\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\gammu_smsd\GammuSendSMS;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides a resource to post sms.
 *
 * @RestResource(
 *   id = "gammu_sms_resource",
 *   label = @Translation("Gammu send SMS"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "https://www.drupal.org/link-relations/create" = "/api/gammu/sms"
 *   }
 * )
 */
class GammuSMSRessource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Gammu send sms service.
   *
   * @var \Drupal\gammu_smsd\GammuSendSMS
   */
  protected GammuSendSMS $gammuSendSMS;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Drupal\gammu_smsd\GammuSendSMS $gammu_send_sms
   *   Gammu send sms service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, AccountProxyInterface $current_user, GammuSendSMS $gammu_send_sms) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
    $this->gammuSendSMS = $gammu_send_sms;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('custom_rest'),
      $container->get('current_user'),
      $container->get('gammu_smsd.sendSMS'),
    );
  }

  /**
   * Responds to POST requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post($data) {
    // Use current user after pass authentication to validate access.
    if (!$this->currentUser->hasPermission('Administer gammu sms')) {
      throw new AccessDeniedHttpException();
    }
    $response_status = NULL;
    if (!empty($data)) {
      $message = [$data['number'], $data['text']];
      $response_status = $this->gammuSendSMS->send([$message]);
    }
    return new ResourceResponse($response_status);
  }

}
