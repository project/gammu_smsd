<?php

namespace Drupal\gammu_smsd;

use Drupal\Core\Database\Database;

/**
 * {@inheritDoc}
 */
class GammuSendSMS extends GammuDatabase {

  /**
   * Variable array $messages have to keys number and text.
   */
  public function send(array $messages) {
    if (!empty($messages)) {
      if (empty($this->connection)) {
        $this->connection = $this->gammuConnection();
      }
      Database::setActiveConnection('gammu_db');
      foreach ($messages as $message) {
        if (empty($message[2])) {
          $message[2] = NULL;
        }
        [$number, $text, $date] = $message;
        if (!empty($number) && !empty($text)) {
          $this->sendSms($number, $text, $date);
        }
      }
      Database::setActiveConnection();
    }
  }

  /**
   * {@inheritDoc}
   */
  public function sendSms($gammu_destinationnumber, $gammu_textdecoded, $sendingDateTime = NULL) {

    $gammu_textdecoded = str_replace(['"', "\r\n", "\n"], ["'", " ", " "], trim($gammu_textdecoded));
    $gammu_textdecoded_split = str_split($gammu_textdecoded, 153);
    $count = count($gammu_textdecoded_split);
    // Send sms with command line.
    if ($this->config->get('gammu_cli')) {
      $gammu_textdecoded = '"' . $gammu_textdecoded . '"';
      if ($count == 1) {
        $cmd = "gammu sendsms TEXT {$gammu_destinationnumber} -text {$gammu_textdecoded}";
        return shell_exec($cmd);
      }
      else {
        $gammu_textdecoded = '"' . $gammu_textdecoded . '"';
        $cmd = "gammu-smsd-inject -c smsdrc TEXT {$gammu_destinationnumber} -unicode -len 400 -text {$gammu_textdecoded}";
        return shell_exec($cmd);
      }
    }
    $creatorID = $this->currentUser->id() ?? 'Drupal Gammu';
    if (empty($this->connection)) {
      $this->connection = $this->gammuConnection();
    }
    // Send sms with mysql.
    if (empty($sendingDateTime)) {
      // 0000-00-00 00:00:00
      $sendingDateTime = date('Y-m-d H:i:s');
    }
    $udhHex = '050003' . dechex(rand(16, 255)) . substr(100 + $count, -2);
    $udh = $udhHex . '01';
    // UDH  050003 D5 02 01.
    if ($count == 1) {
      $query_insert = $this->connection->insert('outbox')->fields([
        'DestinationNumber' => $gammu_destinationnumber,
        'TextDecoded' => $gammu_textdecoded_split[0],
        'CreatorID' => $creatorID,
      ])->execute();
    }
    else {
      // The last inserted id of the table.
      $iD = $this->connection->insert('outbox')->fields([
        'DestinationNumber' => $gammu_destinationnumber,
        'TextDecoded' => $gammu_textdecoded_split[0],
        'UDH' => $udh,
        'MultiPart' => 'true',
        'CreatorID' => $creatorID,
      ])->execute();

      $query_insert = $this->connection->insert('outbox')->fields([
        'UDH',
        'TextDecoded',
        'ID',
        'SequencePosition',
      ]);
      foreach (range(1, $count - 1) as $i) {
        $query_insert->values([
          'UDH' => $udhHex . substr(101 + $i, -2),
          'TextDecoded' => $gammu_textdecoded_split[$i],
          'ID' => $iD,
          'SequencePosition' => $i + 1,
        ]);
      }
      $query_insert->execute();
    }
    return TRUE;
  }

}
