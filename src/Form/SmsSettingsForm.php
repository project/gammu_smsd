<?php

namespace Drupal\gammu_smsd\Form;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Locale\CountryManager;
use Drupal\field\Entity\FieldConfig;
use Drupal\gammu_smsd\GammuSendSMS;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form definition for the salutation message.
 */
class SmsSettingsForm extends ConfigFormBase {

  /**
   * Constructs a new sms settings form.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\gammu_smsd\GammuSendSMS $gammuSendSMS
   *   The gammu send sms service.
   */
  public function __construct(protected EntityFieldManagerInterface $entityFieldManager, protected GammuSendSMS $gammuSendSMS) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('entity_field.manager'),
      $container->get('gammu_smsd.sendSMS'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['gammu_smsd.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sms_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('gammu_smsd.settings');
    $form['gammu_settings_db'] = [
      '#type' => 'details',
      '#title' => $this->t('Database settings'),
      '#open' => TRUE,
    ];

    $form['gammu_settings_db']['gammu_dbhost'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Database Hostname/IP'),
      '#default_value' => $config->get('gammu_dbhost'),
      '#description' => $this->t("Database Hostname or IP, e.g.: localhost, 192.168.0.1, dbase.drupal.org. Leave blank if you use same Hostname for Drupal and Gammu"),
    ];

    $form['gammu_settings_db']['gammu_dbengine'] = [
      '#type' => 'select',
      '#title' => $this->t('Database Engine'),
      '#options' => [
        'mysql' => $this->t('mysql'),
        'pgsql' => $this->t('pgsql'),
        'sqlite' => $this->t('sqlite'),
      ],
      '#default_value' => $config->get('gammu_dbengine'),
      '#description' => $this->t("Database Engine, e.g.: mysql, sqlite, pgsql"),
    ];

    $form['gammu_settings_db']['gammu_dbport'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Database Engine'),
      '#default_value' => $config->get('gammu_dbport'),
      '#description' => $this->t("Database port, e.g.: 3306"),
    ];

    $form['gammu_settings_db']['gammu_dbname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Database Name'),
      '#default_value' => $config->get('gammu_dbname'),
      '#description' => $this->t("Database Name, e.g.: smsd, drpl_123, mydata. Leave blank if you use same Database Name for Drupal and Gammu"),
    ];

    $form['gammu_settings_db']['gammu_dbuser'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Database Username'),
      '#default_value' => $config->get('gammu_dbuser'),
      '#description' => $this->t("Database Username, e.g.: root. Leave blank if you use same Username for Drupal and Gammu"),
    ];

    $form['gammu_settings_db']['gammu_dbpass'] = [
      '#type' => 'password',
      '#title' => $this->t('Database Password'),
      '#default_value' => $config->get('gammu_dbpass'),
      '#description' => $this->t("Database Password, leave blank if you use same database Password for Drupal and Gammu"),
    ];

    $form['gammu_settings_advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced settings'),
      '#open' => FALSE,
    ];

    $form['gammu_settings_advanced']['gammu_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Authentication'),
      '#default_value' => !empty($config->get('gammu_token')) ? $config->get('gammu_token') : substr(md5(rand()), 0, 32),
      '#description' => $this->t("Token for API check authentication send sms via api/gammu/send"),
    ];

    $form['gammu_settings_advanced']['gammu_cli'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use Gammu CLI'),
      '#default_value' => $config->get('gammu_cli'),
      '#description' => $this->t("Use Gammu CLI (Command Line Interface) instead SQL Command. This option requires PHP exec() function."),
    ];

    $form['gammu_settings_test'] = [
      '#type' => 'details',
      '#title' => $this->t('Test Gammu'),
      '#open' => FALSE,
    ];

    $form['gammu_settings_test']['gammu_test_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your mobile number'),
      '#description' => $this->t("Enter your mobile number for testing."),
    ];

    $form['gammu_settings_test']['gammu_test_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('SMS Text'),
      '#default_value' => $this->t('Message test'),
      '#rows' => 2,
    ];

    // Configuration for phone book.
    $form['gammu_settings_phonebook'] = [
      '#type' => 'details',
      '#title' => $this->t('Phonebook'),
      '#open' => TRUE,
      '#description' => $this->t('Mapping field telephone with first name , lastname'),
    ];

    $form['gammu_settings_phonebook']['gammu_country_code'] = [
      '#type' => 'select',
      '#title' => $this->t('Country dial code'),
      '#options' => $this->getCountryDialCode(),
      '#default_value' => $config->get('gammu_country_code'),
    ];
    $options_phone_fields = $this->listPhoneField();
    if (!empty($options_phone_fields)) {
      $form['gammu_settings_phonebook']['gammu_telephone_field'] = [
        '#title' => $this->t('Field telephone'),
        '#type' => 'select',
        '#options' => $options_phone_fields,
        '#default_value' => $config->get('gammu_telephone_field'),
        '#ajax' => [
          'callback' => '::ajaxPhonebookName',
          'wrapper' => 'edit-gammu-settings-phonebook',
          'method' => 'replace',
          'effect' => 'fade',
        ],
      ];
      $form['gammu_settings_phonebook']['gammu_check_status'] = [
        '#title' => $this->t('Skip check status'),
        '#type' => 'checkbox',
        '#default_value' => $config->get('gammu_check_status'),
      ];
      $options_name_fields = $this->getNameField($config->get('gammu_telephone_field'));
      $form['gammu_settings_phonebook']['gammu_name_field'] = [
        '#title' => $this->t('Field Name'),
        '#type' => 'select',
        '#options' => $options_name_fields,
        '#default_value' => $config->get('gammu_name_field'),
        '#empty_option' => $this->t('- Select -'),
      ];
      $form['gammu_settings_phonebook']['gammu_firstname_field'] = [
        '#title' => $this->t('Field First name'),
        '#type' => 'select',
        '#options' => $options_name_fields,
        '#empty_option' => $this->t('- Select -'),
        '#default_value' => $config->get('gammu_firstname_field'),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('gammu_smsd.settings')
      ->set('gammu_dbhost', $form_state->getValue('gammu_dbhost'))
      ->set('gammu_dbengine', $form_state->getValue('gammu_dbengine'))
      ->set('gammu_dbport', $form_state->getValue('gammu_dbport'))
      ->set('gammu_dbname', $form_state->getValue('gammu_dbname'))
      ->set('gammu_dbuser', $form_state->getValue('gammu_dbuser'))
      ->set('gammu_dbpass', $form_state->getValue('gammu_dbpass'))
      ->set('gammu_token', $form_state->getValue('gammu_token'))
      ->set('gammu_cli', $form_state->getValue('gammu_cli'))
      ->set('gammu_country_code', $form_state->getValue('gammu_country_code'))
      ->set('gammu_telephone_field', $form_state->getValue('gammu_telephone_field'))
      ->set('gammu_check_status', $form_state->getValue('gammu_check_status'))
      ->set('gammu_name_field', $form_state->getValue('gammu_name_field'))
      ->set('gammu_firstname_field', $form_state->getValue('gammu_firstname_field'))
      ->save();
    if (!empty($form_state->getValue("gammu_test_number")) && !empty($form_state->getValue("gammu_test_number"))) {
      $messages = [
        $form_state->getValue("gammu_test_number"),
        $form_state->getValue("gammu_test_number"),
      ];
      $this->gammuSendSMS->send($messages);
    }
  }

  /**
   * Covert dial code to name of country.
   */
  public function getCountryDialCode() {
    $countryArray = $this->getCountryInformation();
    $list = CountryManager::getStandardList();
    $dialCode = [];
    foreach ($countryArray as $key => $country) {
      if (!empty($list[$country])) {
        $dialCode[$key] = $list[$country] . ' (' . $key . ')';
      }
    }
    return $dialCode;
  }

  /**
   * {@inheritDoc}
   */
  private function listPhoneField() {
    $options = [];
    $fields_map = $this->entityFieldManager->getFieldMapByFieldType('telephone');
    if (!empty($fields_map)) {
      foreach ($fields_map as $entity_type => $fields) {
        if ($entity_type == 'paragraph') {
          continue;
        }
        foreach ($fields as $field_name => $bundles) {
          $bundle = array_key_first($bundles['bundles']);
          $field = FieldConfig::loadByName($entity_type, $bundle, $field_name);
          $options["$entity_type|$field_name|$bundle"] = $field->getLabel() . " - $bundle";
        }
      }
    }
    return $options;
  }

  /**
   * {@inheritDoc}
   */
  public function ajaxPhonebookName(array &$form, FormStateInterface $form_state) {
    return $form['gammu_settings_phonebook'];
  }

  /**
   * {@inheritDoc}
   */
  private function getNameField($getPhoneField = FALSE) {
    $options = [];
    $request = $this->getRequest();
    $getPhoneField = $request->get("gammu_telephone_field") ?? FALSE;
    if (!empty($getPhoneField)) {
      [$entity_phone_type, $field_phone_name, $phone_bundle] = explode('|', $getPhoneField);
      if (!$field_phone_name) {
      }
    }
    $field_map = $this->entityFieldManager->getFieldMap();
    if (!empty($field_map)) {
      foreach ($field_map as $entity_type => $fields) {
        if (!empty($entity_phone_type) && $entity_phone_type != $entity_type) {
          continue;
        }
        if (in_array($entity_type, [
          'paragraph', 'block_content', 'comment', 'file', 'media',
          'path_alias', 'shortcut', 'contact_message',
        ])) {
          continue;
        }
        foreach ($fields as $field_name => $bundles) {
          if (!in_array($bundles["type"], ['string', 'name', 'address'])) {
            continue;
          }
          $bundle = array_key_first($bundles['bundles']);
          if (in_array($field_name, ['name', 'title']) && !empty($phone_bundle)) {
            $bundle = $phone_bundle;
          }
          if (!empty($phone_bundle) && $bundle != $phone_bundle) {
            continue;
          }
          $field = FieldConfig::loadByName($entity_type, $bundle, $field_name);
          if ($field) {
            $options[$field_name] = $field->getLabel();
          }
          elseif (in_array($field_name, ['name', 'title'])) {
            $options[$field_name] = $this->t('Title');
          }
        }
      }
    }
    return $options;
  }

  /**
   * Get List code dial iso.
   */
  public function getCountryInformation($filter = '') {
    $countryArray = [
      '+376' => "AD",
      '+971' => "AE",
      '+93' => "AF",
      '+1268' => "AG",
      '+1264' => "AI",
      '+355' => "AL",
      '+374' => "AM",
      '+599' => "AN",
      '+244' => "AO",
      '+672' => "AQ",
      '+54' => "AR",
      '+1684' => "AS",
      '+43' => "AT",
      '+61' => "AU",
      '+297' => "AW",
      '+994' => "AZ",
      '+387' => "BA",
      '+1246' => "BB",
      '+880' => "BD",
      '+32' => "BE",
      '+226' => "BF",
      '+359' => "BG",
      '+973' => "BH",
      '+257' => "BI",
      '+229' => "BJ",
      '+590' => "BL",
      '+1441' => "BM",
      '+673' => "BN",
      '+591' => "BO",
      '+55' => "BR",
      '+1242' => "BS",
      '+975' => "BT",
      '+267' => "BW",
      '+375' => "BY",
      '+501' => "BZ",
      '+243' => "CD",
      '+236' => "CF",
      '+242' => "CG",
      '+41' => "CH",
      '+225' => "CI",
      '+682' => "CK",
      '+56' => "CL",
      '+237' => "CM",
      '+86' => "CN",
      '+57' => "CO",
      '+506' => "CR",
      '+53' => "CU",
      '+238' => "CV",
      '+357' => "CY",
      '+420' => "CZ",
      '+49' => "DE",
      '+253' => "DJ",
      '+45' => "DK",
      '+1767' => "DM",
      '+1809' => "DO",
      '+213' => "DZ",
      '+593' => "EC",
      '+372' => "EE",
      '+20' => "EG",
      '+291' => "ER",
      '+34' => "ES",
      '+251' => "ET",
      '+358' => "FI",
      '+679' => "FJ",
      '+500' => "FK",
      '+691' => "FM",
      '+298' => "FO",
      '+33' => "FR",
      '+241' => "GA",
      '+44' => "GB",
      '+1473' => "GD",
      '+995' => "GE",
      '+233' => "GH",
      '+350' => "GI",
      '+299' => "GL",
      '+220' => "GM",
      '+224' => "GN",
      '+240' => "GQ",
      '+30' => "GR",
      '+502' => "GT",
      '+1671' => "GU",
      '+245' => "GW",
      '+592' => "GY",
      '+852' => "HK",
      '+504' => "HN",
      '+385' => "HR",
      '+509' => "HT",
      '+36' => "HU",
      '+62' => "ID",
      '+353' => "IE",
      '+972' => "IL",
      '+91' => "IN",
      '+964' => "IQ",
      '+98' => "IR",
      '+354' => "IS",
      '+39' => "IT",
      '+1876' => "JM",
      '+962' => "JO",
      '+81' => "JP",
      '+254' => "KE",
      '+996' => "KG",
      '+855' => "KH",
      '+686' => "KI",
      '+269' => "KM",
      '+1869' => "KN",
      '+850' => "KP",
      '+82' => "KR",
      '+965' => "KW",
      '+1345' => "KY",
      '+856' => "LA",
      '+961' => "LB",
      '+1758' => "LC",
      '+423' => "LI",
      '+94' => "LK",
      '+231' => "LR",
      '+266' => "LS",
      '+370' => "LT",
      '+352' => "LU",
      '+371' => "LV",
      '+218' => "LY",
      '+212' => "MA",
      '+377' => "MC",
      '+373' => "MD",
      '+382' => "ME",
      '+1599' => "MF",
      '+261' => "MG",
      '+692' => "MH",
      '+389' => "MK",
      '+223' => "ML",
      '+95' => "MM",
      '+976' => "MN",
      '+853' => "MO",
      '+1670' => "MP",
      '+222' => "MR",
      '+1664' => "MS",
      '+356' => "MT",
      '+230' => "MU",
      '+960' => "MV",
      '+265' => "MW",
      '+52' => "MX",
      '+60' => "MY",
      '+258' => "MZ",
      '+264' => "NA",
      '+687' => "NC",
      '+227' => "NE",
      '+234' => "NG",
      '+505' => "NI",
      '+31' => "NL",
      '+47' => "NO",
      '+977' => "NP",
      '+674' => "NR",
      '+683' => "NU",
      '+64' => "NZ",
      '+968' => "OM",
      '+507' => "PA",
      '+51' => "PE",
      '+689' => "PF",
      '+675' => "PG",
      '+63' => "PH",
      '+92' => "PK",
      '+48' => "PL",
      '+508' => "PM",
      '+870' => "PN",
      '+351' => "PT",
      '+680' => "PW",
      '+595' => "PY",
      '+974' => "QA",
      '+40' => "RO",
      '+381' => "RS",
      '+7' => "RU",
      '+250' => "RW",
      '+966' => "SA",
      '+677' => "SB",
      '+248' => "SC",
      '+249' => "SD",
      '+46' => "SE",
      '+65' => "SG",
      '+290' => "SH",
      '+386' => "SI",
      '+421' => "SK",
      '+232' => "SL",
      '+378' => "SM",
      '+221' => "SN",
      '+252' => "SO",
      '+597' => "SR",
      '+239' => "ST",
      '+503' => "SV",
      '+963' => "SY",
      '+268' => "SZ",
      '+1649' => "TC",
      '+235' => "TD",
      '+228' => "TG",
      '+66' => "TH",
      '+992' => "TJ",
      '+690' => "TK",
      '+670' => "TL",
      '+993' => "TM",
      '+216' => "TN",
      '+676' => "TO",
      '+90' => "TR",
      '+1868' => "TT",
      '+688' => "TV",
      '+886' => "TW",
      '+255' => "TZ",
      '+380' => "UA",
      '+256' => "UG",
      '+1' => "US",
      '+598' => "UY",
      '+998' => "UZ",
      '+1784' => "VC",
      '+58' => "VE",
      '+1284' => "VG",
      '+1340' => "VI",
      '+84' => "VN",
      '+678' => "VU",
      '+681' => "WF",
      '+685' => "WS",
      '+967' => "YE",
      '+262' => "YT",
      '+27' => "ZA",
      '+260' => "ZM",
      '+263' => "ZW",
    ];
    // Return.
    return ($filter == '') ? $countryArray : ($countryArray[$filter] ?? '');
  }

}
