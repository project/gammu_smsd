<?php

namespace Drupal\gammu_smsd\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\gammu_smsd\GammuDatabase;
use Drupal\gammu_smsd\GammuSendSMS;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sms compose form.
 */
class SmsComposeForm extends FormBase {

  /**
   * Sms Sent form constructor.
   *
   * @param \Drupal\gammu_smsd\GammuDatabase $gammuDB
   *   Gammu database.
   * @param \Drupal\gammu_smsd\GammuSendSMS $gammuSendSMS
   *   The gammu send sms service.
   */
  public function __construct(protected GammuDatabase $gammuDB, protected GammuSendSMS $gammuSendSMS) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('gammu_smsd.db'),
      $container->get('gammu_smsd.sendSMS'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sms_compose_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $number = '') {
    $form['gammu_number'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Your mobile number'),
      '#rows' => 2,
      '#default_value' => $number,
      '#description' => $this->t("Enter your mobile number separated by comma (,)"),
    ];
    $form['gammu_phonebook'] = [
      '#type' => 'details',
      '#title' => $this->t('Phone book'),
      '#open' => FALSE,
    ];
    $phonebook = $this->gammuDB->gammuGetPhonebook();

    if (!empty($phonebook)) {
      $form['gammu_phonebook']['search'] = [
        '#type' => 'textfield',
        '#title' => '',
        '#attributes' => [
          'placeholder' => $this->t('Search'),
        ],
      ];
      $form['#attached']['library'][] = 'gammu_smsd/gammu_smsd';
      $form['gammu_phonebook']['phone'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Check All'),
        '#options' => $phonebook,
      ];
    }
    $form['#tree'] = TRUE;
    $form['gammu_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('SMS Text'),
      '#rows' => 3,
      '#default_value' => '',
    ];
    $form['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send SMS'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $numbers = explode(',', $form_state->getValue("gammu_number"));
    $text = $form_state->getValue("gammu_text");
    if (!empty($form_state->getValue("gammu_phonebook"))) {
      $phones = array_filter($form_state->getValue("gammu_phonebook")["phone"]);
    }
    if (!empty($phones)) {
      $numbers += $phones;
    }
    if (!empty($text)) {
      $messages = [];
      foreach ($numbers as $number) {
        $messages[] = [$number, $text];
      }
      $this->gammuSendSMS->send($messages);
    }
  }

}
