<?php

namespace Drupal\gammu_smsd;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Database\Database;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Template\TwigEnvironment;

/**
 * {@inheritDoc}
 */
class GammuDatabase {

  use StringTranslationTrait;

  /**
   * Configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Connection database.
   *
   * @var mixed
   */
  protected mixed $connection;

  /**
   * Constructor of gammu.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleExtensionList
   *   The module list service.
   * @param \Drupal\Core\Template\TwigEnvironment $twig
   *   The twig service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, protected AccountProxyInterface $currentUser, protected CacheBackendInterface $cache, protected EntityTypeManagerInterface $entityTypeManager, protected DateFormatterInterface $dateFormatter, protected MessengerInterface $messenger, protected ModuleExtensionList $moduleExtensionList, protected TwigEnvironment $twig) {
    $this->config = $config_factory->get('gammu_smsd.settings');
  }

  /**
   * {@inheritDoc}
   */
  public function gammuConnection() {
    $database = Database::getConnection();
    $gammu_db = $database->getConnectionOptions();
    if (!empty($this->config->get('gammu_dbname'))) {
      $gammu_db['database'] = $this->config->get('gammu_dbname');
    }
    if (!empty($this->config->get('username'))) {
      $gammu_db['username'] = $this->config->get('gammu_dbuser');
    }
    if (!empty($this->config->get('gammu_dbpass'))) {
      $gammu_db['password'] = $this->config->get('gammu_dbpass');
    }
    if (!empty($this->config->get('gammu_dbhost'))) {
      $gammu_db['host'] = $this->config->get('gammu_dbhost');
    }
    if (!empty($this->config->get('gammu_dbhost'))) {
      $gammu_db['driver'] = $this->config->get('gammu_dbengine');
    }
    if (!empty($this->config->get('gammu_dbport'))) {
      $gammu_db['port'] = $this->config->get('gammu_dbport');
    }
    // Replace 'YourDatabaseKey' with something that's unique to your module.
    Database::addConnectionInfo('gammu_db', 'default', $gammu_db);
    try {
      return $this->connection = Database::getConnection('default', 'gammu_db');
    }
    catch (\PDOException $e) {
      $this->messenger->addError($this->t("Connection failed, please contact your administrator"));
      return FALSE;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getMessageMultipart(string $folder, $udh, $num_mobile = '') {
    Database::setActiveConnection('gammu_db');
    $udh = substr($udh, 0, -2);
    $query = $this->connection->select($folder, 'i');
    $query->fields('i', ['TextDecoded']);
    $query->condition('i.UDH', $udh . '%', 'like');
    if ($folder == 'sentitems') {
      $query->condition('i.DestinationNumber', $num_mobile)
        ->orderBy('SequencePosition', 'ASC');
    }
    elseif ($folder == 'inbox') {
      $query->condition('i.UDH', $udh . '%', 'like')->orderBy('ID', 'ASC');
    }
    $results = $query->execute()->fetchCol();
    Database::setActiveConnection();
    return implode('', $results);
  }

  /**
   * {@inheritDoc}
   */
  public function gammuMessageDetail($number, $phone_name, $phone_num, $time) {
    $modulePath = $this->moduleExtensionList->getPath('gammu_smsd');
    $template_file = $modulePath . '/templates/gammu-smsd-message.html.twig';
    $variables = [
      'tel' => $number,
      'phone_name' => $phone_name,
      'phone_num' => $phone_num,
      'time' => $time,
      'time_format' => $this->gammuNiceDate($time),
      'theme_hook_original' => 'not-applicable',
    ];
    return $this->twig->load($template_file)->render($variables);
  }

  /**
   * Helper function for gammu.
   */
  private function gammuNiceDate($date) {
    $date = strtotime($date);
    $interval = time() - $date;
    // Show time ago.
    if ($interval < 60 * 60 * 24 * 2) {
      return $this->dateFormatter->formatInterval($interval);
    }
    return $this->dateFormatter->format($date, 'short');
  }

  /**
   * {@inheritDoc}
   */
  public function gammuGetListPhoneNumber(string $folder) {
    Database::setActiveConnection('gammu_db');
    $query = $this->connection->select($folder, 'i')->distinct();
    switch ($folder) {
      case  'sentitems':
        $query->fields('i', ['DestinationNumber']);
        break;

      case   'inbox':
        $query->fields('i', ['SenderNumber']);
        break;
    }
    $or = $query->orConditionGroup()
      ->condition('i.UDH', '')
      ->condition('i.UDH', '%1', 'like');
    $query->condition($or);
    $telephones = $query->execute()->fetchCol();
    Database::setActiveConnection();
    return $telephones;
  }

  /**
   * {@inheritDoc}
   */
  public function gammuGetPhonebook() {
    $cid = 'gammu_get_phonebook';
    if ($cache = $this->cache->get($cid)) {
      return $cache->data;
    }

    $config = $this->config;
    $phoneField = explode('|', $config->get('gammu_telephone_field') ?? '');
    $entity_type = $phoneField[0] ?? '';
    $field_name = $phoneField[1] ?? '';
    $bundle = $phoneField[2] ?? '';
    $list = [];

    if (!empty($bundle)) {
      $query = $this->entityTypeManager->getStorage($entity_type)->getQuery();
      if (!$config->get("gammu_check_status")) {
        $query->condition('status', TRUE);
      }
      $query->condition('type', $bundle)
        ->condition($field_name, NULL, 'IS NOT NULL');
      $entity_ids = $query->accessCheck()->execute();
      if (empty($entity_ids)) {
        return $list;
      }
      $name_field = $config->get('gammu_name_field');
      $firstname_field = $config->get('gammu_firstname_field');
      if ($firstname_field || $name_field) {
        foreach ($entity_ids as $id) {
          $bundle_load = $this->entityTypeManager->getStorage($entity_type)
            ->load($id);
          if (!empty($bundle_load)) {
            $name = [];
            $name[] = $this->getNameValue($bundle_load, $name_field);
            if (!empty($firstname_field)) {
              $name[] = $this->getNameValue($bundle_load, $firstname_field);
            }
            $telephones = $bundle_load->get($field_name)->value;
            $name = array_unique($name);
            $name = trim(implode(' ', $name));
            $list[$telephones] = !empty($name) ? $name : $telephones;
            unset($name);
          }
        }
      }
    }
    $this->cache->set($cid, $list);
    return $list;
  }

  /**
   * {@inheritDoc}
   */
  private function getNameValue($bundle_load, $name_field) {
    $field = $bundle_load->get($name_field);
    $definition = $field->getDataDefinition();
    $typePlugin = $definition->getType();
    switch ($typePlugin) {
      case 'address':
        $valueAddress = current($field->getValue());
        if (!empty($valueAddress['family_name'])) {
          $name[] = $valueAddress['family_name'];
        }
        if (!empty($valueAddress['given_name'])) {
          $name[] = $valueAddress['given_name'];
        }
        break;

      case 'name':
        $valueName = current($field->getValue());
        if (!empty($valueName['family'])) {
          $name[] = $valueName['family'];
        }
        if (!empty($valueName['given'])) {
          $name[] = $valueName['given'];
        }
        break;

      default:
        $name[] = $field->value;
        break;
    }
    if (!empty($name)) {
      return implode(' ', $name);
    }
    return '';
  }

  /**
   * {@inheritDoc}
   */
  public function gammuDeleteMessage($folder, $ids) {
    $listIDS = [];
    if (is_numeric($ids)) {
      $listIDS = [$ids];
    }
    elseif (is_array($ids)) {
      $listIDS = $ids;
    }
    Database::setActiveConnection('gammu_db');
    $query_delete = $this->connection->delete($folder);

    if ($folder == 'inbox') {
      foreach ($listIDS as $id) {
        // Detect if message is multipart in inbox.
        $query = $this->connection->select($folder, 'i');
        $query->fields('i', ['UDH']);
        $query->condition('i.ID', $id);
        $results = $query->execute()->fetchCol();
        $udh = end($results);
        if (trim($udh) != '') {
          $udh = substr($udh, 0, -2);
          $query_delete->condition('UDH', $udh . '%', 'like');
          $query_delete->execute();
        }
      }
    }
    $query_delete->condition('ID', $listIDS, 'IN');
    $num_delete = $query_delete->execute();
    Database::setActiveConnection();
    return $num_delete;
  }

}
